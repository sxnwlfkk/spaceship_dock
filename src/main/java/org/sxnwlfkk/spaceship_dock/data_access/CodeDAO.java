package org.sxnwlfkk.spaceship_dock.data_access;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.sxnwlfkk.spaceship_dock.data.Code;

import java.util.ArrayList;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

public class CodeDAO extends ImplementDAO<Code> {

    public Code getOne(long id) {
        return super.getOne(id, Code.class);
    }

    public ArrayList<Code> getAll() {
        String hql = "from org.sxnwlfkk.spaceship_dock.data.Code \n";
        hql += "order by id";
        return super.getAll(hql);
    }
}
