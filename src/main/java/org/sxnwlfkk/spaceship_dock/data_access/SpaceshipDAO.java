package org.sxnwlfkk.spaceship_dock.data_access;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.sxnwlfkk.spaceship_dock.data.Spaceship;
import org.sxnwlfkk.spaceship_dock.data.User;

import java.util.ArrayList;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

public class SpaceshipDAO extends ImplementDAO<Spaceship> {

    public Spaceship getOne(long id) {
        return super.getOne(id, Spaceship.class);
    }

    @Override
    public void delete(Spaceship s) {
        s.setDeleted(true);
        update(s);
    }

    public ArrayList<Spaceship> getAll() {
        String hql = "from org.sxnwlfkk.spaceship_dock.data.Spaceship where deleted=false\n";
        hql += "order by id";
        return super.getAll(hql);
    }
}
