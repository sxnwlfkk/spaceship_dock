package org.sxnwlfkk.spaceship_dock.data_access;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.faces.bean.ApplicationScoped;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

@ApplicationScoped
public class DBUtil {

    private SessionFactory sessionFactory;
    private static DBUtil instance;

    private DBUtil () {
        // get sessionFactory
        // Create a configuration instance
        Configuration configuration = new Configuration();

        // Provide configuration file
        configuration.configure("hibernate.cfg.xml");

        // Build a SessionFactory
        sessionFactory = configuration.buildSessionFactory(new StandardServiceRegistryBuilder().configure().build());
    }

    public static DBUtil getInstance() {
        if (instance == null) {
            instance = new DBUtil();
        }
        return instance;
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }

}
