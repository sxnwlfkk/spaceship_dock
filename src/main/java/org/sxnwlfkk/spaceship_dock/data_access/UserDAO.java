package org.sxnwlfkk.spaceship_dock.data_access;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.sxnwlfkk.spaceship_dock.data.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

public class UserDAO extends ImplementDAO<User> {

    public User getOne(long id) {
        return super.getOne(id, User.class);
    }

    public ArrayList<User> getAll() {
        String hql = "from org.sxnwlfkk.spaceship_dock.data.User \n";
        hql += "order by id";
        return super.getAll(hql);
    }

    public boolean checkIfLoginIsCorrect(String username, String password) {
       String hql = "SELECT U FROM org.sxnwlfkk.spaceship_dock.data.User U";
       hql += " WHERE U.userName = '" + username + "' AND U.pwd = '" + password + "'";
       return chechIfItemExists(hql);
    }

    // Seems excessive to use two db query for username and password, but for the moment, it can stay
    public boolean checkIfUserExists(String username) {
        String hql = "SELECT U FROM org.sxnwlfkk.spaceship_dock.data.User U";
        hql += " WHere U.userName = '" + username + "'";

        return chechIfItemExists(hql);
    }

    public boolean checkIfEmailExists(String email) {
        String hql = "SELECT U FROM org.sxnwlfkk.spaceship_dock.data.User U";
        hql += " WHere U.email = '" + email + "'";

        return chechIfItemExists(hql);
    }

    public User getUserWithName(String username) {
        String hql = "SELECT U FROM org.sxnwlfkk.spaceship_dock.data.User U";
        hql += " WHERE U.userName = '" + username + "'";
        ArrayList<User> userList = super.getAll(hql);
        if (!userList.isEmpty()) {
            return userList.get(0);
        }
        return null;
    }

}
