package org.sxnwlfkk.spaceship_dock.data_access;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */
public interface AbstractDAO<T> {

    void delete(T t);
    void update(T t);
    void insert(T t);
    T getOne(long id);
    ArrayList<T> getAll();
    boolean chechIfItemExists(String s);

}
