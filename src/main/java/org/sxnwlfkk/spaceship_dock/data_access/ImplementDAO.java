package org.sxnwlfkk.spaceship_dock.data_access;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sxnwlfkk on 6/28/2017.
 */
public abstract class ImplementDAO<T> implements AbstractDAO<T> {

    public void delete(T t) {
        Session session = DBUtil.getInstance().getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(t);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void update(T t) {
        Session session = DBUtil.getInstance().getSession();
        Transaction tx = null;
        try {
            if (t != null) {
                tx = session.beginTransaction();
                session.update(t);
                tx.commit();
            }
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void insert(T t) {
        Session session = DBUtil.getInstance().getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(t);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public T getOne(long id, Class cls) {
        Session session = DBUtil.getInstance().getSession();
        T t = null;
        try {
            t = (T) session.get(cls, id);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return t;
    }

    public ArrayList<T> getAll(String hql) {
        ArrayList<T> tList = new ArrayList<T>();
        Session session = DBUtil.getInstance().getSession();
        try {
            Query query = session.createQuery(hql);
            tList = (ArrayList<T>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return tList;
    }

    public boolean chechIfItemExists(String hql) {
        Session session = DBUtil.getInstance().getSession();
        Transaction tx = null;
        List result = null;
        try {
            Query query = session.createQuery(hql);
            result = query.list();
            if (result.size() > 0) {
                return true;
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }
}
