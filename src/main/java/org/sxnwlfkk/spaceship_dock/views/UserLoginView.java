package org.sxnwlfkk.spaceship_dock.views;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.sxnwlfkk.spaceship_dock.data_access.UserDAO;

import java.io.Serializable;

@ManagedBean
@SessionScoped
public class UserLoginView implements Serializable {

    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean checkUser() {
//        RequestContext context = RequestContext.getCurrentInstance();
//        FacesMessage message = null;
        boolean loggedIn = false;

        UserDAO u = new UserDAO();
        boolean userInDatabase = u.checkIfLoginIsCorrect(username, password);

        if (userInDatabase) {
            loggedIn = true;
//            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
            return  true;
        } else {
            loggedIn = false;
//            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Logging Error", "Invalid credentials");
            return false;
        }

//        FacesContext.getCurrentInstance().addMessage(null, message);
//        context.addCallbackParam("loggedIn", loggedIn);
    }

    public String login() {

        if (checkUser()) {
            return "hub.xhtml?faces-redirect=true";
        }

        RequestContext.getCurrentInstance().update("growl");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Username of password invalid!"));

        return "";
    }
}
