package org.sxnwlfkk.spaceship_dock.data;

import javax.faces.bean.ManagedBean;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

@ManagedBean
@javax.persistence.Entity
@javax.persistence.Table(name = "ships")
public class Spaceship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "was_in_space")
    private boolean wasInSpace;
    @Column(name = "deleted")
    private boolean deleted;
    @Column(name = "size_of_crew")
    private Integer sizeOfCrew;
    @Column(name = "captain")
    private String captain;
    @Column(name = "type")
    private Integer type;
    @Column(name = "fuel")
    private Integer fuel;
    @Column(name = "number")
    private Integer number;
    @Column(name = "picture")
    private String picture;
    @Column(name = "date_of_creation")
    private Date dateOfCreation;
    @Column(name = "id_of_creator")
    private Long idOfCreator;
    @Column(name = "date_of_last_update")
    private Date dateOfLastUpdate;
    @Column(name = "id_of_last_updater")
    private Long idOfLastUpdater;

    public Spaceship() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isWasInSpace() {
        return wasInSpace;
    }

    public void setWasInSpace(boolean wasInSpace) {
        this.wasInSpace = wasInSpace;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getSizeOfCrew() {
        return sizeOfCrew;
    }

    public void setSizeOfCrew(Integer sizeOfCrew) {
        this.sizeOfCrew = sizeOfCrew;
    }

    public String getCaptain() {
        return captain;
    }

    public void setCaptain(String captain) {
        this.captain = captain;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Long getIdOfCreator() {
        return idOfCreator;
    }

    public void setIdOfCreator(Long idOfCreator) {
        this.idOfCreator = idOfCreator;
    }

    public Date getDateOfLastUpdate() {
        return dateOfLastUpdate;
    }

    public void setDateOfLastUpdate(Date dateOfLastUpdate) {
        this.dateOfLastUpdate = dateOfLastUpdate;
    }

    public Long getIdOfLastUpdater() {
        return idOfLastUpdater;
    }

    public void setIdOfLastUpdater(Long idOfLastUpdater) {
        this.idOfLastUpdater = idOfLastUpdater;
    }
}
