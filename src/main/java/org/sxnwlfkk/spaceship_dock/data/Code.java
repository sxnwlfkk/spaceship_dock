package org.sxnwlfkk.spaceship_dock.data;

import javax.faces.bean.ManagedBean;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

@ManagedBean
@javax.persistence.Entity
@javax.persistence.Table(name = "store")
public class Code {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "type")
    private Integer type;
    @Column(name = "value")
    private String value;
    @Column(name = "date_of_creation")
    private Date dateOfCreation;
    @Column(name = "id_of_creator")
    private Long idOfCreator;
    @Column(name = "date_of_last_update")
    private Date dateOfLastUpdate;
    @Column(name = "id_of_last_updater")
    private Long idOfLastUpdater;

    public Code() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Long getIdOfCreator() {
        return idOfCreator;
    }

    public void setIdOfCreator(Long idOfCreator) {
        this.idOfCreator = idOfCreator;
    }

    public Date getDateOfLastUpdate() {
        return dateOfLastUpdate;
    }

    public void setDateOfLastUpdate(Date dateOfLastUpdate) {
        this.dateOfLastUpdate = dateOfLastUpdate;
    }

    public Long getIdOfLastUpdater() {
        return idOfLastUpdater;
    }

    public void setIdOfLastUpdater(Long idOfLastUpdater) {
        this.idOfLastUpdater = idOfLastUpdater;
    }
}
