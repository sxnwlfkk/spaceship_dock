package org.sxnwlfkk.spaceship_dock.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.sxnwlfkk.spaceship_dock.data.User;
import org.sxnwlfkk.spaceship_dock.data_access.UserDAO;

/**
 * Created by sxnwlfkk on 7/13/2017.
 */

@Service
@Component
public class HibernateDetailsService implements UserDetailsService {

    public HibernateDetailsService() {
        super();
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDAO dao = new UserDAO();
        User user = dao.getUserWithName(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new MyUserPrincipal(user);
    }
}
