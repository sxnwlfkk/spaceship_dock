package org.sxnwlfkk.spaceship_dock.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by sxnwlfkk on 7/13/2017.
 */

public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
