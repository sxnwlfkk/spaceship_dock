package org.sxnwlfkk.spaceship_dock.controller;

import org.primefaces.context.RequestContext;
import org.sxnwlfkk.spaceship_dock.data.Code;
import org.sxnwlfkk.spaceship_dock.data.Spaceship;
import org.sxnwlfkk.spaceship_dock.data.User;
import org.sxnwlfkk.spaceship_dock.data_access.CodeDAO;
import org.sxnwlfkk.spaceship_dock.data_access.SpaceshipDAO;
import org.sxnwlfkk.spaceship_dock.data_access.UserDAO;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by sxnwlfkk on 5/31/2017.
 */

@ManagedBean
@SessionScoped
public class SessionController {

    private CodeDAO codeDAO;
    private SpaceshipDAO spaceshipDAO;
    private UserDAO userDAO;

    ArrayList<Spaceship> spaceships;
    ArrayList<Code> codestore;
    ArrayList<User> users;

    public ArrayList<Spaceship> getSpaceships() {
        return spaceships;
    }

    public void setSpaceships(ArrayList<Spaceship> spaceships) {
        this.spaceships = spaceships;
    }

    public ArrayList<Code> getCodestore() {
        return codestore;
    }

    public void setCodestore(ArrayList<Code> codestore) {
        this.codestore = codestore;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    // Constructor
    public SessionController() {
        codeDAO = new CodeDAO();
        spaceshipDAO = new SpaceshipDAO();
        userDAO = new UserDAO();
    }

    // Spaceships
    public void loadSpaceships() {
        spaceships = spaceshipDAO.getAll();
    }

    public String loadSpaceship(Spaceship s) {
        try {
            Spaceship spaceship = spaceshipDAO.getOne(s.getId());
            System.out.println("The id of the loaded spaceship is " + spaceship.getId());

            // put in the request attribute ... so we can use it on the form page
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("spaceship", spaceship);

        } catch (Exception exc) {
            return null;
        }
        return "update-spaceship-form.xhtml";
    }


    public String updateSpaceship(Spaceship s) {
        spaceshipDAO.update(s);
        return "spaceships?faces-redirect=true";
    }

    public void deleteSpaceship(Spaceship s) {
        spaceshipDAO.delete(s);
    }

    public String addSpaceship(Spaceship s) {
        spaceshipDAO.insert(s);
        return "spaceships?faces-redirect=true";
    }

    // Users
    public void loadUsers() {
        users = userDAO.getAll();
    }

    public String loadUser(User u) {
        try {
            User user = userDAO.getOne(u.getId());
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("user", user);
        } catch (Exception e) { return null; }
        return "update-user-form.xhtml";
    }

    public String updateUser(User u) {
        userDAO.update(u);
        return "users?faces-redirect=true";
    }

    public void deleteUser(User u) {
        userDAO.delete(u);
    }

    public String addUser(User u) {
        userDAO.insert(u);
        return "users?faces-redirect=true";
    }

    public String registerNewUser(User u) {
        RequestContext.getCurrentInstance().update("growl");
        FacesContext context = FacesContext.getCurrentInstance();
        UserDAO uDao = new UserDAO();
        if (uDao.checkIfEmailExists(u.getEmail())) {
            // Tell the user
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "There is an existing account with this email address!"));
            return "";
        } else if (uDao.checkIfUserExists(u.getUserName())) {
            // Tell the user
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "This username is already taken!"));
            return "";
        } else {
            addUser(u);
        }
        return "index?faces-redirect=true";
    }

    // Codestore
    public void loadCodestore() {
        codestore = codeDAO.getAll();
    }

    public String loadCode(Code c) {
        try {
            Code code = codeDAO.getOne(c.getId());
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("code", code);
        } catch (Exception e) { return null; }
        return "update-code-form.xhtml";
    }

    public String updateCode(Code c) {
        codeDAO.update(c);
        return "codestore?faces-redirect=true";
    }

    public void deleteCode(Code c) {
        codeDAO.delete(c);
    }

    public String addCode(Code c) {
        codeDAO.insert(c);
        return "codestore?faces-redirect=true";
    }

}
